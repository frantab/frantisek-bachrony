import * as nodemailer from 'nodemailer';

export interface Attachment {
  filename?: string;
  content?: string;
  path?: string;
  encoding?: string;
  raw?: string;
  href?: string;
  httpHeaders?: unknown;
  contentDisposition?: string;
  contentType?: string;
  headers?: unknown;
  cid?: string;
}

export type EMAIL_DELIVERY_STATE = 'PENDING' | 'PROCESSING' | 'RETRY' | 'SUCCESS' | 'ERROR' | 'FAILURE';

export interface QueuePayload {
  delivery?: {
    state: EMAIL_DELIVERY_STATE;
    attempts: number;
    error?: string;
    info?: {
      messageId: string;
      accepted: string[];
      rejected: string[];
      pending: string[];
    };
  };
  message?: nodemailer.SendMailOptions;
  template?: {
    name: string;
    data?: { [key: string]: unknown };
  };
  to: string[];
  toUids?: string[];
  cc: string[];
  ccUids?: string[];
  bcc: string[];
  bccUids?: string[];
  from?: string;
  replyTo?: string;
  headers?: unknown;
  attachments: Attachment[];
}

export interface TemplateData {
  to?: string;
  name: string;
  subject?: string;
  html?: string;
  text?: string;
  amp?: string;
  partial?: boolean;
  attachments?: Attachment[];
}
