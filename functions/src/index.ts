import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
// TYPES
import {
  TemplateData,
  QueuePayload,
} from './types';

const MAIN_EMAIL = 'frantisek@bachrony.com';

admin.initializeApp();

async function sendMail({
  subject,
  text,
  html,
  attachments = [],
}: TemplateData): Promise<void> {
  const firestore = admin.firestore();

  return new Promise((resolve, reject) => {
    firestore.collection('mail').add({
      to: MAIN_EMAIL,
      message: {
        subject,
        text,
        html,
        attachments,
      },
    }).then((doc) => {
      let attemps = 0;

      const unsubscribe = firestore.collection('mail')
        .doc(doc.id)
        .onSnapshot((snapshot) => {
          attemps += 1;
          const { delivery: { state = '' } = {} } = snapshot.data() as QueuePayload;

          if (state && (state === 'ERROR' || state === 'FAILURE')) {
            if (attemps <= 5) firestore.collection('mail').doc(doc.id).update({ delivery: { state: 'RETRY' } });
            else reject(new Error(`We were unable to send an email. Please contact us on the ${MAIN_EMAIL}.`));
          } else if (state && state === 'SUCCESS') {
            unsubscribe();
            resolve();
          }
        });
    }).catch((error: Error) => reject(error));
  });
}

exports.sendEmail = functions.region('europe-west1').https.onCall(sendMail);
