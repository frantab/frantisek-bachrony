export default {
  title: 'Header',
  nav: {
    about: 'About',
    skills: 'Skills',
    career: 'Career',
    education: 'Education',
    contact: 'Contact',
  },
};
