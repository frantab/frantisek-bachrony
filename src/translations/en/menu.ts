export default {
  about: 'About',
  criterions: 'Criterions',
  criterionsGroups: 'Criterions Groups',
  calculations: 'Calculations',
  formulaStatistics: 'Formula Statistics',
};
