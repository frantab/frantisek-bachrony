import header from './header';
import footer from './footer';
import about from './about';
import skills from './skills';
import career from './career';
import education from './education';
import contact from './contact';
import notFound from './notFound';

export default {
  header,
  footer,
  about,
  skills,
  career,
  education,
  contact,
  notFound,
};
