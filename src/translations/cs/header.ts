export default {
  title: 'Hlavička',
  nav: {
    about: 'O mně',
    skills: 'Znalosti',
    career: 'Kariéra',
    education: 'Vzdělání',
    contact: 'Kontakt',
  },
};
