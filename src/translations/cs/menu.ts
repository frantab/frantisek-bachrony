export default {
  about: 'O mně',
  skills: 'Znalosti',
  career: 'Kariéra',
  education: 'Vzdělání',
  contact: 'Kontakt',
};
