import { createI18n } from 'vue-i18n';
import messages from './translations';

const DEFAULT_LOCALE = 'en';

export const getBrowserLocale: () => string = () => (navigator?.languages ? [navigator.language] : navigator.languages)?.[0]?.split('-')?.[0] || DEFAULT_LOCALE;

export default createI18n({
  locale: getBrowserLocale(),
  fallbackLocale: DEFAULT_LOCALE,
  messages,
});
