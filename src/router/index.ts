import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Cv from '@/views/Cv.vue';
import { RouteNamesInterface } from '@/types';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Cv' as RouteNamesInterface,
    component: Cv,
  },
  {
    path: '/about',
    name: 'About' as RouteNamesInterface,
    component: Cv,
    props: { view: 'About' as RouteNamesInterface },
  },
  {
    path: '/skills',
    name: 'Skills' as RouteNamesInterface,
    component: Cv,
    props: { view: 'Skills' as RouteNamesInterface },
  },
  {
    path: '/career',
    name: 'Career' as RouteNamesInterface,
    component: Cv,
    props: { view: 'Career' as RouteNamesInterface },
  },
  {
    path: '/education',
    name: 'Education' as RouteNamesInterface,
    component: Cv,
    props: { view: 'Education' as RouteNamesInterface },
  },
  {
    path: '/contact',
    name: 'Contact' as RouteNamesInterface,
    component: Cv,
    props: { view: 'Contact' as RouteNamesInterface },
  },
  {
    path: '/*',
    name: 'NotFound' as RouteNamesInterface,
    component: () => import(/* webpackChunkName: "NotFound" */ '../views/NotFound.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
