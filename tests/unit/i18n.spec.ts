import { getBrowserLocale } from '@/i18n';

describe('i18n.ts', () => {
  describe('getBrowserLocale', () => {
    it('returns default language en', () => {
      expect(getBrowserLocale()).toBe('en');
    });

    // TODO - mock navigator and try other locations
  });
});
