export const i18nMock: {
  $t: (translationPath: string) => string;
  $i18n: {
    locale: string;
  };
} = {
  $t: (translationPath: string) => translationPath,
  $i18n: {
    locale: 'en',
  },
};
